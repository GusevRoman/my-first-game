(function () {
  var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
  window.requestAnimationFrame = requestAnimationFrame;
})();



//********************************>>>
//***** Veriables declaration *****>>>
//********************************>>>

var canvas = document.getElementById('canvas');
var realHeight = $('body').height();
var realWidth = $('body').width();
var ctx = canvas.getContext('2d');
var width = realWidth;
var height = realHeight;
var keys = [];
var friction = 0.8;
var gravity = 0.1;
var boxes = [];
var startTime = new Date().getTime();
var isPlaying = true;
var counter = 50;
var time;
var endTime;
var myReq;

var player = {
  x: width / 2,
  y: height - 120,
  width: 80,
  height: 80,
  speed: 3,
  velX: 0,
  velY: 0,
  jumping: false,
  grounded: false
};

var player2 = {
  x: width - 120,
  y: height / 3,
  width: 80,
  height: 80,
  speed: 3,
  velX: 0,
  velY: 0,
  jumping: false,
  grounded: false
};



//******************************>>>
//***** Particles functions *****>>>
//******************************>>>

var particles = [];
var addParticleToArray = function(x, y) {
  particles.push(new createParticle(x, y));
};

//Constructor function
var createParticle = function(xPos, yPos) {
  this.x = xPos;
  this.y = yPos;

	//Generate random velocity
  this.vx = Math.random()*20-10;
  this.vy = -Math.abs(Math.random()*20-10);

  //Generate random color
  var r = Math.random()*255>>0;
  var g = Math.random()*255>>0;
  var b = Math.random()*255>>0;
  this.color = 'rgba('+r+', '+g+', '+b+', 0.5)';

  //Generate random size (20px-30px)
  this.radius = Math.random()*20+20;
};

//Define canvas with real pixels
canvas.width = width;
canvas.height = height;



//**************************>>>
//***** Images for game *****>>>
//**************************>>>

//Backround
var background = new Image();
background.src = '/img/background.jpg';

//Player1
var jsImage = new Image();
jsImage.src = '/img/js.png';

//Player2
var htmlImage = new Image();
htmlImage.src = '/img/html.png';



//*********************>>>
//***** Game Walls *****>>>
//*********************>>>

boxes.push({x: 0, y: 0, width: 10, height: height / 1.2}, {x: 0, y: height - 10, width: width, height: 10}, {x: width - 10, y: height / 2, width: 10, height: height});
boxes.push({x: width / 2, y: height / 2, width: width / 2, height: 10}, {x: width / 3, y: height / 1.5, width: width / 2, height: 10}, {x: 0, y: height / 1.2, width: width / 2, height: 10}, {x: 0, y: height / 3, width: width / 2, height: 10}, {x: width / 4, y: height / 6, width: width / 2, height: 10}, {x: width / 4 + width / 2, y: height / 6, width: 10, height: height / 2});



//*************************>>>
//***** Game Functions *****>>>
//*************************>>>

//Update gravity
var updateGravity = function() {
  gravity = $('.gravity-input').val() * 0.1;
};

//Restart
var restartPositions = function() {
  startTime = new Date().getTime();
  player.x = width / 2;
  player.y = height - 120;
  player2.x = width - 120;
  player2.y = height / 3;
  player.jumping = false;
  player2.jumping = false;
  player.velX = 0;
  player.velY = 0;
  player2.velX = 0;
  player2.velY = 0;
};

//Show timer
var drawTime = function(stopped) {
  ctx.font = '48px serif';
  ctx.fillStyle = '#00CCFF';
  if (stopped) {
    time = endTime;
  } else {
    time = (new Date().getTime()-startTime)/1000;
  }
  time = time.toFixed(1);
  ctx.fillText(time + ' sec', width / 1.7, height / 1.7);
};

//Start Game
var startGame = function() {
  startTime = new Date().getTime();
  $('.rules-wrapper').hide();
  game();
};


//*************************>>>
//***** GO GO GO *****>>>
//*************************>>>

var game = function() {

  //***** Keyboard check *****>>>
  if (keys[38] || keys[32]) {
    //Up arrow or space
    if (!player.jumping && player.grounded) {
      player.jumping = true;
      player.grounded = false;
      player.velY = -player.speed * 2;
    }
    if (!player2.jumping && player2.grounded) {
      player2.jumping = true;
      player2.grounded = false;
      player2.velY = -player2.speed * 2;
    }
  }
  if (keys[39]) {
    //Right arrow
    if (player.velX < player.speed) {
      player.velX++;
    }
    if (player2.velX < player2.speed) {
      player2.velX++;
    }
  }
  if (keys[37]) {
    //Left arrow
    if (player.velX > -player.speed) {
      player.velX--;
    }
    if (player2.velX > -player2.speed) {
      player2.velX--;
    }
  }

  //***** Update position *****>>>
  player.velX *= friction;
  player.velY += gravity / 4;
  player2.velX *= friction;
  player2.velY += gravity / 4;
  player.grounded = false;
  player2.grounded = false;


  //***** Canvas clear *****>>>
  ctx.clearRect(0, 0, width, height);
  ctx.drawImage(background,0,0);
  ctx.beginPath();
  ctx.fillStyle = '#00CCFF';


  //***** Draw walls *****>>>
  for (var i = 0; i < boxes.length; i++) {
    ctx.rect(boxes[i].x, boxes[i].y, boxes[i].width, boxes[i].height);

    //Check collisions
    var dir = colCheck(player, boxes[i]);
    var dir2 = colCheck(player2, boxes[i]);

    if (dir === 'l' || dir === 'r') {
      player.velX = 0;
      player.jumping = false;
    } else if (dir === 'b') {
      player.grounded = true;
      player.jumping = false;
    } else if (dir === 't') {
      player.velY *= -1;
    }

    if (dir2 === 'l' || dir2 === 'r') {
      player2.velX = 0;
      player2.jumping = false;
    } else if (dir2 === 'b') {
      player2.grounded = true;
      player2.jumping = false;
    } else if (dir2 === 't') {
      player2.velY *= -1;
    }
  }
  ctx.fill();

  if(player.grounded){
    player.velY = 0;
  }
  if(player2.grounded){
    player2.velY = 0;
  }


  //***** Check if players are at the same location *****>>>
  if (Math.abs(player.x - player2.x) < 5 && Math.abs(player.y - player2.y) < 5) {

    //Change isPlaying and update time
    if (isPlaying) {
      isPlaying = false;
      endTime = (new Date().getTime()-startTime)/1000;
    }

    drawTime('stopped');

    //Draw circles 5 times slower than loop
    if (counter % 5 == 0) {
      addParticleToArray(player.x + 50, player.y + 50);
    }
    counter++;

    //Draw particles
    for(var t = 0; t < particles.length; t++) {
      var p = particles[t];
      ctx.beginPath();
      //Color gradient and drawing
      var gradient = ctx.createRadialGradient(p.x, p.y, 0, p.x, p.y, p.radius);
      gradient.addColorStop(0, p.color);
      gradient.addColorStop(0.5, p.color);
      gradient.addColorStop(1, 'transparent');
      ctx.fillStyle = gradient;
      ctx.arc(p.x, p.y, p.radius, Math.PI*2, false);
      ctx.fill();

      //Update position and add gravity
      p.x += p.vx;
      p.y += p.vy;
      p.vy += gravity / 4;
    }
  } else {

    //If there is no collision between players - update players
    player.x += player.velX;
    player.y += player.velY;
    player2.x += player2.velX;
    player2.y += player2.velY;
    drawTime(null);
  }

  //***** Draw players *****>>>
  ctx.fillStyle = 'rgba(255, 255, 255, 0)';

  //Helper rectangles
  ctx.fillRect(player.x, player.y, player.width, player.height);
  ctx.fillRect(player2.x, player2.y, player2.width, player2.height);

  //Draw Images
  ctx.drawImage(jsImage, player.x, player.y, 80, 90);
  ctx.drawImage(htmlImage, player2.x, player2.y, 80, 80);

  //Go go go
  myReq = requestAnimationFrame(game);
};



//***********************************>>>
//***** Collision check function *****>>>
//***********************************>>>

var colCheck = function(shapeA, shapeB) {
  //Get the vectors to check against
  var vX = (shapeA.x + (shapeA.width / 2)) - (shapeB.x + (shapeB.width / 2));
  var vY = (shapeA.y + (shapeA.height / 2)) - (shapeB.y + (shapeB.height / 2));
  //Add the half widths and half heights of the objects
  var hWidths = (shapeA.width / 2) + (shapeB.width / 2);
  var hHeights = (shapeA.height / 2) + (shapeB.height / 2);
  var colDir = null;

  //If the x and y vector are less than the half width or half height, they we must be inside the object, causing a collision
  if (Math.abs(vX) < hWidths && Math.abs(vY) < hHeights) {
      //Figures out on which side we are colliding (top, bottom, left, or right)
    var oX = hWidths - Math.abs(vX);
    var oY = hHeights - Math.abs(vY);
    if (oX >= oY) {
      if (vY > 0) {
        colDir = 't';
        shapeA.y += oY;
      } else {
        colDir = 'b';
        shapeA.y -= oY;
      }
    } else {
      if (vX > 0) {
        colDir = 'l';
        shapeA.x += oX;
      } else {
        colDir = 'r';
        shapeA.x -= oX;
      }
    }
  }
  return colDir;
};



//************************>>>
//***** EventListners *****>>>
//************************>>>

document.body.addEventListener('keydown', function (e) {
  keys[e.keyCode] = true;
});

document.body.addEventListener('keyup', function (e) {
  keys[e.keyCode] = false;
});
